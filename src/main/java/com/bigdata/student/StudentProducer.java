package com.bigdata.student;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.Properties;

public class StudentProducer {
    public static void main(String[] args) {
        StudentProducer producer = new StudentProducer();
        producer.writeMessage();
    }

    public void writeMessage() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //use the Student Serializer
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StudentSerializer.class);
        //create a student object and send it        
        Student student = new Student(2345, "Paul");
        Producer<String, Student> producer = new KafkaProducer<>(properties);
        //prepare and send record
        ProducerRecord<String, Student> studentRecord = new ProducerRecord<>("student", null, student);
        producer.send(studentRecord);
        //ensures record is sent before closing the producer
        producer.flush();
        producer.close();
    }
}
