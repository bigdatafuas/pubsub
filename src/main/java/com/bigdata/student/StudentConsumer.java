package com.bigdata.student;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class StudentConsumer {

    public static void main(String[] args) {
        StudentConsumer consumer = new StudentConsumer();
        consumer.readMessage();
    }

    public void readMessage() {
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StudentDeserializer.class);
        
        Consumer<String, Student> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList("student"));

        Duration timeout = Duration.ofMillis(100);
        try {
            while (true) {
                ConsumerRecords<String, Student> records = consumer.poll(timeout);
                for (ConsumerRecord<String, Student> student_record : records) {
                    System.out.printf("name = %s, matNum = %i  %n", student_record.value().getName(), student_record.value().getMatNum());
                }
            }
        } 
        finally
        {
            consumer.close();
        }
    }
}
