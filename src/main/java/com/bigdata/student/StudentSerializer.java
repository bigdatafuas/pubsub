package com.bigdata.student;

import java.util.Map;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;
public class StudentSerializer implements Serializer<Student> {
    @Override
    public void configure(Map<String,?> configs, boolean isKey) {
        // nothing to do
    }

    @Override
    public byte[] serialize(String topic, Student data) {
        try {
            return data.serialize();
        } catch (Exception e) {
            throw new SerializationException("Error when serializing Customer to byte[] " + e);
        }
    }
    
    @Override
    public void close() 
    {
        // nothing to do
    }
}