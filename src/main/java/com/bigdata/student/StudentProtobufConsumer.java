package com.bigdata.student;

import com.bigdata.student.StudentProtos.StudentMessage;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializer;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufDeserializerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.common.serialization.StringDeserializer;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

public class StudentProtobufConsumer {

    public static void main(String[] args) {
        StudentProtobufConsumer consumer = new StudentProtobufConsumer();
        consumer.readMessage();
    }

    public void readMessage() {
        //set consumer properties
        Properties properties = new Properties();
        properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, KafkaProtobufDeserializer.class);
        //for protobuf a group id is required
        properties.put(ConsumerConfig.GROUP_ID_CONFIG, "consumer-group");
        //protobuf requires a registry server
        properties.put(SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");
        //set the Protobuf mesage, otherwise a generic DynamicMessage is received
        properties.put(KafkaProtobufDeserializerConfig.SPECIFIC_PROTOBUF_VALUE_TYPE, StudentMessage.class.getName());

        //create and subscribe
        Consumer<String, StudentMessage> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Collections.singletonList("protobuf-student"));

        Duration timeout = Duration.ofMillis(100);
        try {
            while (true) {
                ConsumerRecords<String, StudentMessage> records = consumer.poll(timeout);
                for (ConsumerRecord<String, StudentMessage> student_record : records) {
                    Student student = new Student(student_record.value().getMatNum(), student_record.value().getName()); 
                    System.out.printf("name = %s, matNum = %d  %n", student.getName(), student.getMatNum());
                }
                consumer.commitSync();
            }
        }
        finally 
        {
            consumer.close();
        }
    }
}
