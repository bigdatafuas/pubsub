package com.bigdata.student;

import java.util.Map;
import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;
public class StudentDeserializer implements Deserializer<Student> {
    @Override
    public void configure(Map<String,?> configs, boolean isKey) {
        // nothing to do
    }

    @Override
    public Student deserialize(String topic, byte [] data) {
        try {
            return new Student(data);
        } catch (Exception e) {
            throw new SerializationException("Error when serializing Student to byte[] " + e);
        }
    }
    
    @Override
    public void close() 
    {
        // nothing to do
    }
}