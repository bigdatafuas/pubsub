package com.bigdata.student;

import com.bigdata.student.StudentProtos.StudentMessage;
import io.confluent.kafka.serializers.protobuf.KafkaProtobufSerializer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import static io.confluent.kafka.serializers.AbstractKafkaSchemaSerDeConfig.SCHEMA_REGISTRY_URL_CONFIG;

import java.util.Properties;

public class StudentProtobufProducer {

    public static void main(String[] args) {
        StudentProtobufProducer protobufProducer = new StudentProtobufProducer();
        protobufProducer.writeMessage();
    }

    public void writeMessage() {
        Properties properties = new Properties();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaProtobufSerializer.class);
        properties.put(SCHEMA_REGISTRY_URL_CONFIG, "http://localhost:8081");

        Producer<String, StudentMessage> producer = new KafkaProducer<>(properties);
        Student student = new Student(1234, "Peter");

        //prepare and send the kafka record
        ProducerRecord<String, StudentMessage> student_record = new ProducerRecord<>("protobuf-student", null, student.getStudentMessage());
        producer.send(student_record);
        producer.flush(); //ensures record is sent before closing the producer
        producer.close();
    }
}
