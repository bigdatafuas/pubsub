package com.bigdata.student;

import com.bigdata.student.StudentProtos.StudentMessage;

import static java.nio.charset.StandardCharsets.UTF_8;

import java.nio.ByteBuffer;

public class Student {
    private int matNum;
    private String studentName;
    
    public Student(int num, String name) {
        this.matNum = num;
        this.studentName = name;
    }
   
    public int getMatNum() {
        return matNum;
    }
    
    public String getName() {
        return studentName;
    }

    public Student(byte [] data) throws Exception {
        this.deserialize(data);
    }

    /**
     * Converts the student object into a byte sequence for transmission
     */
    public byte[] serialize() throws Exception {
        try {
            byte[] serializedName = this.getName().getBytes(UTF_8); //use UTF-8 encoding
            int stringSize = serializedName.length;
            int sizeOfInt = 4; // 4 bytes in Java
            //create and fill a byte buffer for the matNum, the length of the name, and the name
            ByteBuffer buffer = ByteBuffer.allocate(sizeOfInt + sizeOfInt + stringSize);
            buffer.putInt(this.getMatNum());
            // the string has a variable length, so we have to transmit it, too
            buffer.putInt(stringSize); 
            buffer.put(serializedName);
            return buffer.array();
        } catch (Exception e) {
            throw new Exception("Error when serializing Student to byte[] " + e);
        } 
    }

    /**
     * Deserializes a byte[], e.g. received from the network.
     */
    public void deserialize(byte[] data) throws Exception {
        int stringSize;
        try {
            ByteBuffer buffer = ByteBuffer.wrap(data);
            this.matNum = buffer.getInt();
            stringSize = buffer.getInt();
            byte[] nameBytes = new byte[stringSize];
            buffer.get(nameBytes);
            this.studentName = new String(nameBytes, UTF_8);
        } catch (Exception e) {
            throw new Exception("Error when deserializing byte[] to Student" + e);
        } 
    }

    /**
     * Returns the protobuf serialization
     */ 
    public StudentMessage getStudentMessage() {
        //build the message
        StudentMessage.Builder studentMsgBuilder = StudentMessage.newBuilder();
        studentMsgBuilder.setName(this.getName());
        studentMsgBuilder.setMatNum(this.getMatNum());
        //return the serialization
        return studentMsgBuilder.build();
    }
} 