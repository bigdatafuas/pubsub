# Introduction

This project gives an introduction to Kafka serialization and de-serialization with and with protobuf.


Inspired by https://github.com/codingharbour/kafka-protobuf

# Build
```
mvn clean generate-sources
mvn compile
```

# Run
Start Kafka and the schema registry, e.g. use single-node-avro-kafka example https://github.com/codingharbour/kafka-docker-compose

```
mvn exec:java -Dexec.mainClass=com.bigdata.student.StudentProtobufProducer
```